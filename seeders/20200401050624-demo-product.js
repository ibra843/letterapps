"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Produks",
      [
        {
          nama_barang: "rinso",
          unit: "2 kg",	
          jumlah_barang: "10", 
          harga_perunit:"10000",
          total_harga:"100000",
          HET: "SK Men perindag"
        },
        {
          nama_barang: "beras",
          unit: "2 kg",	
          jumlah_barang: "10", 
          harga_perunit:"20000",
          total_harga:"200000",
          HET: "SK Men perindag"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Produks", null, {});
  }
};
