'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Orders', [{
        asal_order: 'lumpang',
        tanggal_pesanan: new Date(),
        nomor_pesanan :'',
        provinsi: 'dki jakarta',
        tanggal_jemput_barang: new Date(),
        no_surat_pemesanan: 'kdgfsdbfs',
        tgl_surat_pemesanan: new Date(),
        kota_pemesan: 'jakarta',
        list_barang_id: 1,
        total_harga: '800000'
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Orders', null, {});
  }
};
