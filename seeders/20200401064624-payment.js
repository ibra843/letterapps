'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Payments', [{
        order_id: 1,
        tgl_bayar: new Date(),
        total_bayar: '7000',
        tagihan: '90000',
        balance: "Tunggakan"
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Payments', null, {});
  }
};
