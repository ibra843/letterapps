'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('List_Barangs', [{
        b1: 1,
        b2: 2,
        b3: null,
        b4: null,
        b5: null,
        b6: null,
        b7: null,
        b8: null,
        b9: null,
        b10: null
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('List_Barangs', null, {});
  }
};
