const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.send("hello world");
});

const { getOne_payment, Add_payment } = require("../controllers/payment");
const { getOne_order, Add_order } = require("../controllers/order");
const {
  getOne_listBarang,
  Add_listBarang, edit_listBarang
} = require("../controllers/list_barang");
const { getOne_produk, Add_produk, Edit_produk } = require("../controllers/produk");

// route payment
router.get("/payment/:id", getOne_payment);
router.post("/payment", Add_payment);

// route order
router.get("/order/:id", getOne_order);
router.post("/order", Add_order);

// route list barang
router.get("/listBarang/:id", getOne_listBarang);
router.post("/listBarang", Add_listBarang);
// router.put("/listBarang/:id", edit_listBarang);  

// route product
router.get("/produk/:id", getOne_produk);
router.post("/produk", Add_produk);
router.put("/produk/:id", Edit_produk);

module.exports = router;
