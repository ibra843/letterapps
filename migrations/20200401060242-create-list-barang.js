'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('List_Barangs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      b1: {
        type: Sequelize.INTEGER
      },
      b2: {
        type: Sequelize.INTEGER
      },
      b3: {
        type: Sequelize.INTEGER
      },
      b4: {
        type: Sequelize.INTEGER
      },
      b5: {
        type: Sequelize.INTEGER
      },
      b6: {
        type: Sequelize.INTEGER
      },
      b7: {
        type: Sequelize.INTEGER
      },
      b8: {
        type: Sequelize.INTEGER
      },
      b9: {
        type: Sequelize.INTEGER
      },
      b10: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('List_Barangs');
  }
};