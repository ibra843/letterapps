'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Orders', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      asal_order: {
        type: Sequelize.STRING
      },
      tanggal_pesanan: {
        type: Sequelize.DATE
      },
      nomor_pesanan: {
        type: Sequelize.STRING
      },
      provinsi: {
        type: Sequelize.STRING
      },
      tanggal_jemput_barang: {
        type: Sequelize.DATE
      },
      no_surat_pemesanan: {
        type: Sequelize.STRING
      },
      tgl_surat_pemesanan: {
        type: Sequelize.DATE
      },
      kota_pemesan: {
        type: Sequelize.STRING
      },
      list_barang_id: {
        type: Sequelize.INTEGER
      },
      total_harga: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Orders');
  }
};