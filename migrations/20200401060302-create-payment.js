"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("Payments", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      order_id: {
        type: Sequelize.INTEGER
      },
      tgl_bayar: {
        type: Sequelize.DATE
      },
      total_bayar: {
        type: Sequelize.STRING
      },
      tagihan: {
        type: Sequelize.STRING
      },
      balance: {
        type: Sequelize.ENUM("Lunas", "Tunggakan")
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("Payments");
  }
};
