'use strict';
module.exports = (sequelize, DataTypes) => {
  const List_Barang = sequelize.define('List_Barang', {
    b1: DataTypes.INTEGER,
    b2: DataTypes.INTEGER,
    b3: DataTypes.INTEGER,
    b4: DataTypes.INTEGER,
    b5: DataTypes.INTEGER,
    b6: DataTypes.INTEGER,
    b7: DataTypes.INTEGER,
    b8: DataTypes.INTEGER,
    b9: DataTypes.INTEGER,
    b10: DataTypes.INTEGER
  }, {});
  List_Barang.associate = function(models) {
    // associations can be defined here
    List_Barang.belongsTo(models.Produk, {
      as: "barang_1",
      foreignKey: "b1"
    });
    List_Barang.belongsTo(models.Produk, {
      as: "barang_2",
      foreignKey: "b2"
    });
    List_Barang.belongsTo(models.Produk, {
      as: "barang_3",
      foreignKey: "b3"
    });
    List_Barang.belongsTo(models.Produk, {
      as: "barang_4",
      foreignKey: "b4"
    });
    List_Barang.belongsTo(models.Produk, {
      as: "barang_5",
      foreignKey: "b5"
    });
    List_Barang.belongsTo(models.Produk, {
      as: "barang_6",
      foreignKey: "b6"
    });
    List_Barang.belongsTo(models.Produk, {
      as: "barang_7",
      foreignKey: "b7"
    });
    List_Barang.belongsTo(models.Produk, {
      as: "barang_8",
      foreignKey: "b8"
    });
    List_Barang.belongsTo(models.Produk, {
      as: "barang_9",
      foreignKey: "b9"
    });
    List_Barang.belongsTo(models.Produk, {
      as: "barang_10",
      foreignKey: "b10"
    });
  };
  return List_Barang;
};