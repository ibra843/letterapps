"use strict";
module.exports = (sequelize, DataTypes) => {
  const Payment = sequelize.define(
    "Payment",
    {
      order_id: DataTypes.INTEGER,
      tgl_bayar: DataTypes.DATE,
      total_bayar: DataTypes.STRING,
      tagihan: DataTypes.STRING,
      balance: DataTypes.ENUM("Lunas", "Tunggakan")
    },
    {}
  );
  Payment.associate = function(models) {
    // associations can be defined here
    Payment.belongsTo(models.Order, {
      as: "order",
      foreignKey: "order_id"
    });
  };
  return Payment;
};
