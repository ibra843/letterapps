'use strict';
module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define('Order', {
    asal_order: DataTypes.STRING,
    tanggal_pesanan: DataTypes.DATE,
    nomor_pesanan: DataTypes.STRING,
    provinsi: DataTypes.STRING,
    tanggal_jemput_barang: DataTypes.DATE,
    no_surat_pemesanan: DataTypes.STRING,
    tgl_surat_pemesanan: DataTypes.DATE,
    kota_pemesan: DataTypes.STRING,
    list_barang_id: DataTypes.INTEGER,
    total_harga: DataTypes.STRING
  }, {});
  Order.associate = function(models) {
    // associations can be defined here
    Order.belongsTo(models.List_Barang, {
      as: "list_barang",
      foreignKey: "list_barang_id"
    });
  };
  return Order;
};