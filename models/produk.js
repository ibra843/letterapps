'use strict';
module.exports = (sequelize, DataTypes) => {
  const Produk = sequelize.define('Produk', {
    nama_barang: DataTypes.STRING,
    unit: DataTypes.STRING,
    jumlah_barang: DataTypes.STRING,
    harga_perunit: DataTypes.STRING,
    total_harga:DataTypes.STRING,
    het: DataTypes.STRING
  }, {});
  Produk.associate = function(models) {
    // associations can be defined here
  };
  return Produk;
};