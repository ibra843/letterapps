const models = require("../models");
const Payments = models.Payment;
const Orders = models.Order;
const List_barangs = models.List_Barang;

exports.getOne_payment = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await Payments.findOne({
      where: { id },
      include: [
        {
          model: Orders,
          as: "order",
          attributes: [
            "asal_order",
            "tanggal_pesanan",
            "nomor_pesanan",
            "provinsi",
            "tanggal_jemput_barang",
            "no_surat_pemesanan",
            "tgl_surat_pemesanan",
            "kota_pemesan",
            "total_harga"
          ],
          include: [
            {
              model: List_barangs,
              as: "list_barang",
              attributes: [
                  "b1",
                  "b2",
                  "b3",
                  "b4",
                  "b5",
                  "b6",
                  "b7",
                  "b8",
                  "b9",
                  "b10"
              ]
            }
          ]

        }
      ],
      attributes: { exclude: ["createdAt", "updatedAt"] }
    });
    res.status(200).send({ message: "success to find data", data });
  } catch (err) {
    console.log(err);
  }
};


exports.Add_payment = async (req, res) => {
  const {
    order_id,
    tgl_bayar,
    total_bayar,
    tagihan,
    balance
  } = req.body;
  try {
    const payments = await Payments.create({
      order_id,
      tgl_bayar,
      total_bayar,
      tagihan,
      balance
    });
    const id = payments.id;
    const data = await Payments.findOne({
      where: { id },
      attributes: { exclude: ["createdAt", "updatedAt"] }
    });
    res.status(200).send({ message: "success", data });
  } catch (err) {
    console.log(err);
  }
};
