const models = require("../models");
const Produks = models.Produk;
const List = models.List_Barang;

// for get one data from produk
exports.getOne_produk = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await Produks.findOne({
      where: { id },
      attributes: { exclude: ["spesies", "createdAt", "updatedAt"] }
    });
    res.send({ message: "success GetAll data spesies", data });
  } catch (err) {
    console.log(err);
  }
};

// for add data in produk
exports.Add_produk = async (req, res) => {
  const {
    values,
    values2,
    values3,
    values4,
    values5,
    values6,
    values7,
    values8,
    values9,
    values10
  } = req.body;

  try {
    if (values2 === undefined) {
      const produk = await Produks.create(values);
      const list = await List.create({ b1: produk.id });
      res.status(200).send({ message: "success", data: produk, list });
    } else if (values3 === undefined) {
      const produk = await Produks.create(values, values2);
      const list = await List.create({ b1: produk[0].id, b2: produk[1].id });
      res.status(200).send({ message: "success", data: produk, list });
    } else if (values4 === undefined) {
      const produk = await Produks.create(values, values2, values3);
      const list = await List.create({
        b1: produk[0].id,
        b2: produk[1].id,
        b3: produk[2].id
      });
      res.status(200).send({ message: "success", data: produk, list });
    } else if (values5 === undefined) {
      const produk = await Produks.create(values, values2, values3, values4);
      const list = await List.create({
        b1: produk[0].id,
        b2: produk[1].id,
        b3: produk[2].id,
        b4: produk[3].id
      });
      res.status(200).send({ message: "success", data: produk, list });
    } else if (values6 === undefined) {
      const produk = await Produks.create(
        values,
        values2,
        values3,
        values4,
        values5
      );
      const list = await List.create({
        b1: produk[0].id,
        b2: produk[1].id,
        b3: produk[2].id,
        b4: produk[3].id,
        b5: produk[4].id
      });
      res.status(200).send({ message: "success", data: produk, list });
    } else if (values7 === undefined) {
      const produk = await Produks.create(
        values,
        values2,
        values3,
        values4,
        values5,
        values6
      );
      const list = await List.create({
        b1: produk[0].id,
        b2: produk[1].id,
        b3: produk[2].id,
        b4: produk[3].id,
        b5: produk[4].id,
        b6: produk[5].id
      });
      res.status(200).send({ message: "success", data: produk, list });
    } else if (values8 === undefined) {
      const produk = await Produks.create(
        values,
        values2,
        values3,
        values4,
        values5,
        values6,
        values7
      );
      const list = await List.create({
        b1: produk[0].id,
        b2: produk[1].id,
        b3: produk[2].id,
        b4: produk[3].id,
        b5: produk[4].id,
        b6: produk[5].id,
        b7: produk[6].id
      });
      res.status(200).send({ message: "success", data: produk, list });
    } else if (values9 === undefined) {
      const produk = await Produks.create(
        values,
        values2,
        values3,
        values4,
        values5,
        values6,
        values7,
        values8
      );
      const list = await List.create({
        b1: produk[0].id,
        b2: produk[1].id,
        b3: produk[2].id,
        b4: produk[3].id,
        b5: produk[4].id,
        b6: produk[5].id,
        b7: produk[6].id,
        b8: produk[7].id
      });
      res.status(200).send({ message: "success", data: produk, list });
    
    } else {
      const produk = await Produks.bulkCreate([
        values,
        values2,
        values3,
        values4,
        values5,
        values6,
        values7,
        values8,
        values9
      ]);
      const list = await List.create({
        b1: produk[0].id,
        b2: produk[1].id,
        b3: produk[2].id,
        b4: produk[3].id,
        b5: produk[4].id,
        b6: produk[5].id,
        b7: produk[6].id,
        b8: produk[7].id,
        b9: produk[8].id
      });
      res.status(200).send({ message: "success", data: produk, list });
      console.log("values 2 gk kosong asdkasdkas[dasd]");
    }
  } catch (err) {
    console.log(err);
  }
};

// for edit data produk
exports.Edit_produk = async (req, res) => {
  const {
    nama_barang,
    unit,
    jumlah_barang,
    harga_perunit,
    total_harga
  } = req.body;
  // const train = req.body.train_id;
  try {
    const id = req.params.id;
    const produk = await Produks.update(
      {
        nama_barang,
        unit,
        jumlah_barang,
        harga_perunit,
        total_harga
      },
      { where: { id } }
    );
    if (produk.length > 0 && produk[0]) {
      const data = await Produks.findOne({
        where: { id },
        attributes: { exclude: ["createdAt", "updatedAt"] }
      });
      res.status(200).send({ message: "succes to update data", data });
    } else {
      res.status(404).send({ message: "success" });
    }
  } catch (err) {
    console.log(err);
  }
};
