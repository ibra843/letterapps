const models = require("../models");
const Orders = models.Order;
const List_barangs = models.List_Barang;
const Produks = models.Produk;

exports.getOne_order = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await Orders.findOne({
      where: { id },
      include: [
        {
          model: List_barangs,
          as: "list_barang",
          attributes: ["id"],
          include: [
            {
              model: Produks,
              as: "barang_1",
              attributes: [
                "nama_barang",
                "unit",
                "jumlah_barang",
                "harga_perunit",
                "total_harga",
                "het"
              ]
            }
          ]
        }
      ],

      attributes: { exclude: ["list_barang_id", "createdAt", "updatedAt"] }
    });
    res.status(200).send({ message: "success to find data", data });
  } catch (err) {
    console.log(err);
  }
};

exports.Add_order = async (req, res) => {
  const {
    asal_order,
    tanggal_pesanan,
    nomor_pesanan,
    provinsi,
    tanggal_jemput_barang,
    no_surat_pemesanan,
    tgl_surat_pemesanan,
    kota_pemesan,
    list_barang_id,
    total_harga
  } = req.body;
  try {
    const order = await Orders.create({
      asal_order,
      tanggal_pesanan,
      nomor_pesanan,
      provinsi,
      tanggal_jemput_barang,
      no_surat_pemesanan,
      tgl_surat_pemesanan,
      kota_pemesan,
      list_barang_id,
      total_harga
    });
    const id = order.id;
    const data = await Orders.findOne({
      where: { id },
      attributes: { exclude: ["createdAt", "updatedAt"] }
    });
    res.status(200).send({ message: "success", data });
  } catch (err) {
    console.log(err);
  }
};
