const models = require("../models");
const List_barangs = models.List_Barang;
const Produks = models.Produk;

exports.getOne_listBarang = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await List_barangs.findOne({
      where: { id },
      include: [
        {
          model: Produks,
          as: "barang_1",
          attributes: [
            "nama_barang",
            "unit",
            "jumlah_barang",
            "harga_perunit",
            "total_harga",
            "het"
          ]
        },
        {
          model: Produks,
          as: "barang_2",
          attributes: [
            "nama_barang",
            "unit",
            "jumlah_barang",
            "harga_perunit",
            "total_harga",
            "het"
          ]
        },
        {
          model: Produks,
          as: "barang_3",
          attributes: [
            "nama_barang",
            "unit",
            "jumlah_barang",
            "harga_perunit",
            "total_harga",
            "het"
          ]
        },
        {
          model: Produks,
          as: "barang_4",
          attributes: [
            "nama_barang",
            "unit",
            "jumlah_barang",
            "harga_perunit",
            "total_harga",
            "het"
          ]
        },
        {
          model: Produks,
          as: "barang_5",
          attributes: [
            "nama_barang",
            "unit",
            "jumlah_barang",
            "harga_perunit",
            "total_harga",
            "het"
          ]
        },
        {
          model: Produks,
          as: "barang_6",
          attributes: [
            "nama_barang",
            "unit",
            "jumlah_barang",
            "harga_perunit",
            "total_harga",
            "het"
          ]
        },
        {
          model: Produks,
          as: "barang_7",
          attributes: [
            "nama_barang",
            "unit",
            "jumlah_barang",
            "harga_perunit",
            "total_harga",
            "het"
          ]
        },
        {
          model: Produks,
          as: "barang_8",
          attributes: [
            "nama_barang",
            "unit",
            "jumlah_barang",
            "harga_perunit",
            "total_harga",
            "het"
          ]
        },
        {
          model: Produks,
          as: "barang_9",
          attributes: [
            "nama_barang",
            "unit",
            "jumlah_barang",
            "harga_perunit",
            "total_harga",
            "het"
          ]
        },
        {
          model: Produks,
          as: "barang_10",
          attributes: [
            "nama_barang",
            "unit",
            "jumlah_barang",
            "harga_perunit",
            "total_harga",
            "het"
          ]
        }
      ],
      attributes: {
        exclude: [
          "id",
          "b1",
          "b2",
          "b3",
          "b4",
          "b5",
          "b5",
          "b6",
          "b7",
          "b8",
          "b9",
          "b10",
          "createdAt",
          "updatedAt"
        ]
      }
    });
    res.status(200).send({ message: "success to find data", data });
  } catch (err) {
    console.log(err);
  }
};

// for add data in produk
exports.Add_listBarang = async (req, res) => {
  const { b1, b2, b3, b4, b5, b6, b7, b8, b9, b10 } = req.body;
  try {
    const list_barang = await List_barangs.create({
      b1,
      b2,
      b3,
      b4,
      b5,
      b6,
      b7,
      b8,
      b9,
      b10
    });
    const id = list_barang.id;
    const data = await List_barangs.findOne({
      where: { id },
      attributes: { exclude: ["createdAt", "updatedAt"] }
    });
    res.status(200).send({ message: "success", data });
  } catch (err) {
    console.log(err);
  }
};


// for edit data produk
exports.Edit_produk = async (req, res) => {
  const {
      b1,
      b2,
      b3,
      b4,
      b5,
      b6,
      b7,
      b8,
      b9,
      b10
  } = req.body;
  // const train = req.body.train_id;
  try {
    const id = req.params.id;
    const produk = await Produks.update(
      {
        b1,
      b2,
      b3,
      b4,
      b5,
      b6,
      b7,
      b8,
      b9,
      b10
      },
      { where: { id } }
    );
    if (produk.length > 0 && produk[0]) {
      const data = await Produks.findOne({
        where: { id },
        attributes: { exclude: ["createdAt", "updatedAt"] }
      });
      res.status(200).send({ message: "succes to update data", data });
    } else {
      res.status(404).send({ message: "success" });
    }
  } catch (err) {
    console.log(err);
  }
};